.. _avail_mappings:

3D mapped domains
-----------------

This page lists the currently available geometries in Struphy:

- `available mapped domains <https://struphy.pages.mpcdf.de/struphy/sections/STUBDIR/struphy.geometry.domains.html>`_

Domain-related code stands in the following sub-modules:

.. currentmodule:: ''

.. autosummary::
    :nosignatures:
    :toctree: STUBDIR

    struphy.geometry.base
    struphy.geometry.domains
    struphy.geometry.utilities


.. inheritance-diagram:: struphy.geometry.domains
    :parts: 1


Base classes
^^^^^^^^^^^^

.. automodule:: struphy.geometry.base
    :members:
    :special-members:
    :show-inheritance:
    :exclude-members: __init__


Available domains
^^^^^^^^^^^^^^^^^

.. automodule:: struphy.geometry.domains
    :members:
    :exclude-members: kind_map, params_map, params_numpy, pole, periodic_eta3
    :show-inheritance:


.. _field_tracing:

Utilities
^^^^^^^^^

.. automodule:: struphy.geometry.utilities
    :members:
    :show-inheritance: