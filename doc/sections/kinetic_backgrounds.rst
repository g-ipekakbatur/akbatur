.. _kinetic_backgrounds:

Kinetic initial conditions
--------------------------

This page lists the currently available kinetic backgrounds/initial conditions in Struphy:

- `Maxwellians <https://struphy.pages.mpcdf.de/struphy/sections/STUBDIR/struphy.kinetic_background.maxwellians.html>`_


Documented modules:

.. currentmodule:: ''

.. autosummary::
    :nosignatures:
    :toctree: STUBDIR

    struphy.kinetic_background.base
    struphy.kinetic_background.maxwellians


Base classes
^^^^^^^^^^^^

.. automodule:: struphy.kinetic_background.base
    :members:
    :undoc-members: 
    :show-inheritance:


Available Maxwellians
^^^^^^^^^^^^^^^^^^^^^

.. inheritance-diagram:: struphy.kinetic_background.maxwellians
    :parts: 1
    
.. automodule:: struphy.kinetic_background.maxwellians
    :members:
    :undoc-members: 
    :show-inheritance:

