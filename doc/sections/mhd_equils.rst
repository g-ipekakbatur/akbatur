.. _mhd_equil:

MHD equilibria
--------------

This page lists the currently available MHD equilibira in Struphy:

- `available MHD equilibira <https://struphy.pages.mpcdf.de/struphy/sections/STUBDIR/struphy.fields_background.mhd_equil.equils.html>`_

Documented modules:

.. currentmodule:: ''

.. autosummary::
    :nosignatures:
    :toctree: STUBDIR

    struphy.fields_background.mhd_equil.base
    struphy.fields_background.mhd_equil.equils

    
.. _mhd_base:

Base classes
^^^^^^^^^^^^

.. automodule:: struphy.fields_background.mhd_equil.base
    :members:
    :undoc-members: 
    :exclude-members: 
    :show-inheritance:


.. _mhd_equil_avail:

Available MHD equilibria
^^^^^^^^^^^^^^^^^^^^^^^^

.. inheritance-diagram:: struphy.fields_background.mhd_equil.equils
    :parts: 1

.. automodule:: struphy.fields_background.mhd_equil.equils
    :members:
    :undoc-members:
    :show-inheritance:





