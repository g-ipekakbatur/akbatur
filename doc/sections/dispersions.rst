.. _disp_rels:

Dispersion relations
--------------------

This page lists the currently available 1D dispersion relations in Struphy:

- `analytic <https://struphy.pages.mpcdf.de/struphy/sections/STUBDIR/struphy.dispersion_relations.analytic.html>`_


Documented modules:

.. currentmodule:: ''

.. autosummary::
    :nosignatures:
    :toctree: STUBDIR

    struphy.dispersion_relations.base
    struphy.dispersion_relations.analytic


Base classes
^^^^^^^^^^^^

.. automodule:: struphy.dispersion_relations.base
    :members:
    :undoc-members:
    :show-inheritance:


Available dispersion relations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. inheritance-diagram:: struphy.dispersion_relations.analytic
    :parts: 1

.. automodule:: struphy.dispersion_relations.analytic
    :members:
    :undoc-members:
    :show-inheritance: