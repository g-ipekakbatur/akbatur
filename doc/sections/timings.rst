.. _timings:

Timings
=======

This page contains the latest timings and memory usage of

* Struphy :ref:`unit tests <timings_unit>`
* Struphy :ref:`propagators tests <timings_props>`
* Struphy :ref:`models quick tests <timings_models>`
* Struphy :ref:`tutorial runs <timings_tutorials>`

on the runners provided by `gitlab.mpcdf.mpg.de <https://gitlab.mpcdf.mpg.de/help/ci/runners/runners_scope.md#shared-runners>`_. 
Both ``C`` and ``Fortran`` compilations are timed.


.. _timings_unit:

Unit tests
----------

Language C
^^^^^^^^^^

**Contexts** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_unit_contexts_c.html

|

**Sessions** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_unit_sessions_c.html

|

**Metrics** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_unit_metrics_c.html

Language Fortran
^^^^^^^^^^^^^^^^

**Contexts** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_unit_contexts_fortran.html

|

**Sessions** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_unit_sessions_fortran.html

|

**Metrics** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_unit_metrics_fortran.html


.. _timings_props:

Propagators
-----------

Language C
^^^^^^^^^^

**Contexts** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_propagators_contexts_c.html

|

**Sessions** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_propagators_sessions_c.html

|

**Metrics** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_propagators_metrics_c.html

Language Fortran
^^^^^^^^^^^^^^^^

**Contexts** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_propagators_contexts_fortran.html

|

**Sessions** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_propagators_sessions_fortran.html

|

**Metrics** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_propagators_metrics_fortran.html


.. _timings_models:

Models
------

Language C
^^^^^^^^^^

**Contexts** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_models_contexts_c.html

|

**Sessions** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_models_sessions_c.html

|

**Metrics** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_models_metrics_c.html

Language Fortran
^^^^^^^^^^^^^^^^

**Contexts** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_models_contexts_fortran.html

|

**Sessions** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_models_sessions_fortran.html

|

**Metrics** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_models_metrics_fortran.html


.. _timings_tutorials:

Tutorials
---------

Language C
^^^^^^^^^^

**Contexts** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_tutorials_contexts_c.html

|

**Sessions** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_tutorials_sessions_c.html

|

**Metrics** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_tutorials_metrics_c.html

Language Fortran
^^^^^^^^^^^^^^^^

**Contexts** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_tutorials_contexts_fortran.html

|

**Sessions** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_tutorials_sessions_fortran.html

|

**Metrics** (one for each MPI rank):

.. raw:: html
   :file: ../../src/struphy/_pymon/pymon_tutorials_metrics_fortran.html