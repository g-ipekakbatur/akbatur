{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6 - Mapped domains with polar singularity\n",
    "\n",
    "In this tutorial we will consider [Struphy domains](https://struphy.pages.mpcdf.de/struphy/sections/domains.html) that have a polar singularity (\"magnetic axis\"). Such singularities are challenging numerically; in Struphy two solutions are possible:\n",
    "\n",
    "1. Use [polar splines](https://struphy.pages.mpcdf.de/struphy/sections/developers.html?highlight=polar%20splines#derham-sequence-3d) when setting up the de Rham sequence (=more expensive)\n",
    "2. Cut out a small hole of the domain around the singularity (=cheap, but problematic for particles).\n",
    "\n",
    "We shall focus on the second possibility in this notebook.\n",
    "\n",
    "## HollowCylinder\n",
    "\n",
    "Let us create the domain [HollowCylinder](https://struphy.pages.mpcdf.de/struphy/sections/domains.html#struphy.geometry.domains.HollowCylinder) with default parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.geometry import domains\n",
    "\n",
    "domain = domains.HollowCylinder()\n",
    "domain.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The default parameters of `HollowCylinder` are:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for key, val in domain.params_map.items():\n",
    "    print(key, '=', val)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some relevant domain attributes are:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(domain.kind_map)\n",
    "print(domain.pole)\n",
    "print(domain.periodic_eta3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The domain methods are also quite important:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for attr in dir(domain):\n",
    "    if callable(getattr(domain, attr)) and '__' not in attr and attr[0] != '_':\n",
    "        print(attr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Aside from these, the domain object itself is callable: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(domain.__call__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us change the size of the hole around the pole:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "domain = domains.HollowCylinder(a1=.05)\n",
    "domain.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that if we set the inner radius `a1` to zero, the attribute `domain.pole` becomes `True`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "domain = domains.HollowCylinder(a1=.0)\n",
    "domain.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(domain.kind_map)\n",
    "print(domain.pole)\n",
    "print(domain.periodic_eta3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## HollowTorus\n",
    "\n",
    "Let us create the domain [HollowTorus](https://struphy.pages.mpcdf.de/struphy/sections/domains.html#struphy.geometry.domains.HollowTorus) with default parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "domain = domains.HollowTorus()\n",
    "domain.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the attribute `periodic_eta3` is `True` for this mapping:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(domain.kind_map)\n",
    "print(domain.pole)\n",
    "print(domain.periodic_eta3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The default parameters are:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for key, val in domain.params_map.items():\n",
    "    print(key, '=', val)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us change the size of the hole around the pole, the poloidal angle parametrization (`sfl`)  and the `tor_period`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "domain = domains.HollowTorus(a1=.05, sfl=True, tor_period=1)\n",
    "domain.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tokamak\n",
    "\n",
    "[Tokamak](https://struphy.pages.mpcdf.de/struphy/sections/domains.html#struphy.geometry.domains.Tokamak) is the class for mappings for Tokamak MHD equilibria constructed via [field-line tracing](https://struphy.pages.mpcdf.de/struphy/sections/domains.html#struphy.geometry.utilities.field_line_tracing) of a poloidal flux function $\\psi$.\n",
    "\n",
    "Let us create a Tokamak with default parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "domain = domains.Tokamak()\n",
    "domain.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The default parameters are:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for key, val in domain.params_map.items():\n",
    "    if 'cx' not in key and 'cy' not in key:\n",
    "        print(key, '=', val)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ``Tokamak`` domain is always related to an [AxisymmMHDequilibrium](https://struphy.pages.mpcdf.de/struphy/sections/mhd_equils.html#struphy.fields_background.mhd_equil.base.AxisymmMHDequilibrium), which provides the flux function $\\psi$. In the default parameters this is [AdhocTorus](https://struphy.pages.mpcdf.de/struphy/sections/mhd_equils.html#struphy.fields_background.mhd_equil.equils.AdhocTorus). Instead, we could also look at the default [EQDSKequilibrium](https://struphy.pages.mpcdf.de/struphy/sections/mhd_equils.html#struphy.fields_background.mhd_equil.equils.EQDSKequilibrium):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.fields_background.mhd_equil.equils import EQDSKequilibrium\n",
    "\n",
    "mhd_eq = EQDSKequilibrium()\n",
    "\n",
    "domain = domains.Tokamak(equilibrium=mhd_eq)\n",
    "domain.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us shrink the hole:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "domain = domains.Tokamak(equilibrium=mhd_eq, psi_shifts=[.2, 2])\n",
    "domain.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stellarator mappings\n",
    "\n",
    "Struphy can read data produced by the [GVEC equilibrium code](https://gitlab.mpcdf.mpg.de/gvec-group/gvec). The interface is the class [GVECunit](https://struphy.pages.mpcdf.de/struphy/sections/domains.html?highlight=gvec#struphy.geometry.domains.GVECunit).\n",
    "\n",
    "Let us create an instance with defautl parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "domain = domains.GVECunit()\n",
    "domain.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The default parameters are:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for key, val in domain.params_map.items():\n",
    "    if 'cx' not in key and 'cy' not in key and 'cz' not in key:\n",
    "        print(key, '=', val)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The mapping parameters contain the spline mapping data and the related [GVECequilibrium](https://struphy.pages.mpcdf.de/struphy/sections/mhd_equils.html#struphy.fields_background.mhd_equil.equils.GVECequilibrium), which is specified through the parameter file.\n",
    "Let us check the parameters of the default GVEC equilibrium:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for key, val in domain.params_map['equilibrium'].params.items():\n",
    "    print(key, val)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us put a domain hole around the magnetic axis and use the whole Stellarator (`use_nfp=False`). The parameters must be passed through the [GVECequilibrium](https://struphy.pages.mpcdf.de/struphy/sections/mhd_equils.html#struphy.fields_background.mhd_equil.equils.GVECequilibrium):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.fields_background.mhd_equil.equils import GVECequilibrium\n",
    "\n",
    "gvec_equil = GVECequilibrium(rmin=.1, use_nfp=False)\n",
    "domain = domains.GVECunit(gvec_equil)\n",
    "domain.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
