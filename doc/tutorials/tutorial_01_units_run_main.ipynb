{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1 - Run Struphy main file in a notebook\n",
    "\n",
    "This tutorial is about the Struphy main execution file `struphy/main.py`. The file is executed from the console upon calling\n",
    "```\n",
    "    struphy run MODEL\n",
    "```\n",
    "Please visit https://struphy.pages.mpcdf.de/struphy/sections/userguide.html for detailed information about this command. In this tutorial, we shall\n",
    "\n",
    "1. Import `struphy/main.py` and look at its functionality.\n",
    "2. Create some default prameter files and change some parameters.\n",
    "3. Understand the normalization of Struphy models (which units are used).\n",
    "3. Run the model [LinearMHDVlasovCC](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.hybrid.LinearMHDVlasovCC) in the notebook (without invoking the console).\n",
    "\n",
    "## Main execution file and parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.main import main\n",
    "\n",
    "help(main)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `main.py` has three mandatory arguments:\n",
    "\n",
    "- `model_name`\n",
    "- `parameters`\n",
    "- `path_out`\n",
    "\n",
    "Available Struphy models for `model_name` can be listed from the console:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!{'struphy run -h'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Struphy, parameters are passed to a model via a dictionary that is stored in `.yml` format (the \"parameter file\") in the current input path. The current I/O paths can be obtained via"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!{'struphy -p'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us change the current I/O paths to the default:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!{'struphy --set-iob d'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the default input path, there is one \"core\" parameter file `parameters.yml` that contains all 9 possible top-level keys mentioned in the [Struphy userguide](https://struphy.pages.mpcdf.de/struphy/sections/userguide.html#setting-simulation-parameters):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import struphy\n",
    "import yaml\n",
    "\n",
    "core_name = os.path.join(struphy.__path__[0], 'io/inp', 'parameters.yml')\n",
    "\n",
    "with open(core_name) as file:\n",
    "    core_params = yaml.load(file, Loader=yaml.FullLoader)\n",
    "    \n",
    "for key, val in core_params.items():\n",
    "    print(key, ':')\n",
    "    print(val, '\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Model-specific parameter files must be created from the core file via the console. For example,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!{'struphy params Maxwell -y'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "leads to the creation of `params_Maxwell.yml` in the current input path:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pfile = os.path.join(struphy.__path__[0], 'io/inp', 'params_Maxwell.yml')\n",
    "\n",
    "with open(pfile) as file:\n",
    "    params_Maxwell = yaml.load(file, Loader=yaml.FullLoader)\n",
    "    \n",
    "for key, val in params_Maxwell.items():\n",
    "    print(key, ':')\n",
    "    print(val, '\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each model has its specific structure of the parameter file, created from the core file with the command `struphy params MODEL`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!{'struphy params -h'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " The model parameter files can differ in top-level keys, variable names and the `options` available to each species. The available options for a model are accessible via the corresponding class method, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.models.toy import Maxwell\n",
    "for key, val in Maxwell.options().items():\n",
    "    print(key, ':')\n",
    "    print(val, '\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the command line, this information can be accessed via"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!{'struphy params Maxwell --options'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The possible choices of the options are given as lists within the options dictionary ; the first list entry is the default value.  The key structure of the options dictionary is exactly the same as it appears in the parameter file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(params_Maxwell['em_fields']['options']['solver'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "In this tutorial, we shall simulate the current coupling hybrid model [LinearMHDVlasovCC](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.hybrid.LinearMHDVlasovCC). Let us set the `model_name` argument for the `main` file, look at the options of this model and create the default parameter file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_name = 'LinearMHDVlasovCC'\n",
    "\n",
    "from struphy.models.hybrid import LinearMHDVlasovCC\n",
    "for key, val in LinearMHDVlasovCC.options().items():\n",
    "    print(key, ':')\n",
    "    print(val, '\\n')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!{'struphy params LinearMHDVlasovCC -y --file tutor_01.yml'}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pfile = os.path.join(struphy.__path__[0], 'io/inp', 'tutor_01.yml')\n",
    "\n",
    "with open(pfile) as file:\n",
    "    parameters = yaml.load(file, Loader=yaml.FullLoader)\n",
    "    \n",
    "for key, val in parameters.items():\n",
    "    print(key, ':')\n",
    "    print(val, '\\n')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Struphy normalization (units)\n",
    "\n",
    "In this section we shall gain an understanding of the units used in Struphy (model normalization). \n",
    "\n",
    "In the present example, the geometry is a `Cuboid` with default parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.geometry.domains import Cuboid\n",
    "\n",
    "cube = Cuboid()\n",
    "\n",
    "cube.params_map"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The side length in direction `i = 1,2,3` is given by `ri - li`. The question arises in which unit of length these numbers are expressed (meters, millimeters, light years?). From the console, the units can be checked via"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!{'struphy units LinearMHDVlasovCC -i tutor_01.yml'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, two informations are passed to `struphy units`, namely the model name (`LinearMHDVlasovCC`) and the parameter file (`tutor_01.yml`). The former is obvious because each Struphy model has its own specific normalization, stated in the model's documentation (and docstring). The latter, however, is not obvious (parameters influence the units?). **Indeed, Struphy provides the flexibility that the units of each model can be influenced by the user via the parameter file.**\n",
    "\n",
    "Let us check the relevant section in the dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['units']"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, the user can set\n",
    "\n",
    "1. the unit of length $\\hat x$ in meter\n",
    "2. the unit of the magnetic field strength $\\hat B$ in Tesla\n",
    "3. the unit of the number density $\\hat n$ in $10^{20}$ $m^{-3}$.\n",
    "\n",
    "In the above example we have $\\hat x = 1\\,m$, $\\hat B = 1\\,T$ and $\\hat n = 10^{20}$ $m^{-3}$. "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " All other units, such as for velocity $\\hat v$ or time $\\hat t$ etc., are derived from the three basic units above. How is this achieved? In Struphy, each model has the two class methods\n",
    " \n",
    "- `velocity_scale`\n",
    "- `bulk_species`\n",
    "\n",
    "These have been set by the model developer (hard-coded) and cannot be changed by the user. They determine the derived units in the following way:\n",
    "\n",
    "The `bulk_species` sets the mass number ($A$) and charge number ($Z$) to be used in the calculation of units:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.models.hybrid import LinearMHDVlasovCC\n",
    "\n",
    "print(LinearMHDVlasovCC.bulk_species())\n",
    "\n",
    "parameters['fluid'][LinearMHDVlasovCC.bulk_species()]['phys_params']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "The `velocity_scale` (partly) determines the velocity unit $\\hat v$. It has been set by the model developer to one of the following:\n",
    "\n",
    "1. speed of light, $\\hat v = c$\n",
    "2. Alfvén speed of the bulk species, $\\hat v = v_\\textnormal{A, bulk} = \\sqrt{\\hat B^2 / (m_\\textnormal{bulk} \\hat n \\mu_0)}$\n",
    "3. Cyclotron speed of the bulk species, $\\hat v = \\hat x \\Omega_\\textnormal{c, bulk}/(2\\pi) = \\hat x\\, q_\\textnormal{bulk} \\hat B /(m_\\textnormal{bulk}2\\pi)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(LinearMHDVlasovCC.velocity_scale())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "The three possible velocities scales are entirely defined in terms of:\n",
    "\n",
    "- the three units $\\hat x$, $\\hat B$, $\\hat n$, which are provided by the user (who can thus also influence $\\hat v$)\n",
    "- the `bulk_species` (through $m_\\textnormal{bulk} = m_\\textnormal{proton} A$ and $q_\\textnormal{bulk} = q_\\textnormal{e}Z$). \n",
    "\n",
    "The associated time scale is then automatically given by\n",
    "$$\n",
    " \\hat t = \\hat x / \\hat v \\,.\n",
    "$$\n",
    "\n",
    " To summarize: qualitatively, the `velocity_scale` and the `bulk_species` are fixed within each model by the developer (hard-coded). Quantitatively, the values (here for the Alfvén speed and the MHD charge and mass) are set by the user through the parameter file. "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please check out https://struphy.pages.mpcdf.de/struphy/sections/models.html#normalization for further discussion on the units used in Struphy. In this tutorial, instead of the console, we can inspect the units of our run also directly in this notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "units, equation_params = LinearMHDVlasovCC.model_units(parameters, verbose=True)\n",
    "units"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Aside from the units, there are also the `equation_params` returned, namely\n",
    "\n",
    "- `alpha_unit` being the ratio of the unit plasma frequency to the unit cyclotron frequency\n",
    "- `epsilon_unit` being the ratio of the unit angular frequency to the unit cyclotron frequency\n",
    "\n",
    "for each species."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "equation_params"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run Struphy main\n",
    "\n",
    "Let us get back to the parameter file and change some entries in the parameter dictionary before we run the model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['grid']['Nel'] = [18, 14, 4]\n",
    "parameters['kinetic']['energetic_ions']['phys_params']['A'] = 4."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now ready to call the Struphy main file. A tutorial of how to post-process the generated simulation data is available [here](https://struphy.pages.mpcdf.de/struphy/doc/_build/html/tutorials/tutorial_02_postproc_standard_plotting.html). \n",
    "\n",
    "The simulation results will be stored in the default output path under the folder `tutorial_01/`:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_out = os.path.join(struphy.__path__[0], 'io/out', 'tutorial_01')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us finally call `main`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "main(model_name, parameters, path_out)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The previous call was equivalent to the console command\n",
    "```\n",
    "struphy run LinearMHDVlasovCC -i tutor_01.yml -o tutorial_01\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
