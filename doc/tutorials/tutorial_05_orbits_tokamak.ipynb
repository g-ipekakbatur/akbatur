{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5 - Tokamak particle orbits\n",
    "\n",
    "In this tutorial we will plot particle orbits in poloidal cross sections of Tokamaks.\n",
    "\n",
    "The Struphy run generating the data for this tutorial can be launched via:\n",
    "\n",
    "    $ struphy test tutorials -n 5\n",
    "    \n",
    "All data generated in this way is stored in the Struphy installation path (obtained from ``struphy -p``) under ``io/out/tutorial_05``.\n",
    "\n",
    "## Vlasov vs. DriftKinetic\n",
    "\n",
    "We shall consider the two models [Vlasov](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.toy.Vlasov) and [DriftKinetic](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.toy.DriftKinetic) next to each other."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import struphy\n",
    "\n",
    "path_out = os.path.join(struphy.__path__[0], 'io/out/', 'tutorial_05a/')\n",
    "\n",
    "os.listdir(path_out)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us get some meta data on the simulations used in this notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(os.path.join(path_out, 'meta.txt')) as file:\n",
    "    print(file.read())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us look at the parameter file of the simulation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params_path = os.path.join(struphy.__path__[0], 'io/inp', 'tutorials', 'params_05a.yml')\n",
    "\n",
    "import yaml\n",
    "\n",
    "with open(params_path) as file:\n",
    "    parameters = yaml.load(file, Loader=yaml.FullLoader)\n",
    "    \n",
    "parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Under the key word `kinetic` we see that only 4 particles were simulated, each one with carefully chosen initial conditions (`markers/loading/initial`).\n",
    "\n",
    "The raw data has already been post-processed (presence of the folder `post_processing/`). Let us look at the resulting data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.listdir(os.path.join(path_out, 'post_processing'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us start by loading the time grid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "tgrid = np.load(os.path.join(path_out, 'post_processing', 't_grid.npy'))\n",
    "Nt = len(tgrid) - 1\n",
    "log_Nt = int(np.log10(Nt)) + 1\n",
    "\n",
    "Nt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.listdir(os.path.join(path_out, 'post_processing', 'kinetic_data'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.listdir(os.path.join(path_out, 'post_processing', 'kinetic_data', 'ions'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from tqdm import tqdm\n",
    "\n",
    "Np = parameters['kinetic']['ions']['save_data']['n_markers']\n",
    "print(Np)\n",
    "    \n",
    "pos = np.zeros((Nt + 1, Np, 3), dtype=float)\n",
    "\n",
    "for n in tqdm(range(Nt + 1)):\n",
    "\n",
    "    # load x, y, z coordinates\n",
    "    orbits_path = os.path.join(path_out, 'post_processing', 'kinetic_data', 'ions', 'orbits', 'ions_{0:0{1}d}.txt'.format(n, log_Nt))\n",
    "    pos[n] = np.loadtxt(orbits_path, delimiter=',')[:, 1:]\n",
    "\n",
    "    # convert to R, y, z, coordinates\n",
    "    pos[n, :, 0] = np.sqrt(pos[n, :, 0]**2 + pos[n, :, 1]**2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At first, we want to plot the absolute value of the magnetic field in the poloidal plane.\n",
    "\n",
    "We thus start by creating the `Domain` and `MHDequilibirum` objects from the parameter file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.io.setup import setup_domain_mhd\n",
    "\n",
    "domain, mhd_equil = setup_domain_mhd(parameters)\n",
    "domain_name = domain.__class__.__name__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The absolute value of $B$ at toroidal angle $\\phi=0$ looks as follows (we create one plot for `Vlasov` and one plot for `DriftKinetic`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt\n",
    "\n",
    "fig = plt.figure(figsize=(16, 8))\n",
    "ax1 = fig.add_subplot(1, 2, 1)\n",
    "ax2 = fig.add_subplot(1, 2, 2)\n",
    "\n",
    "e1 = np.linspace(0., 1., 101)\n",
    "e2 = np.linspace(0., 1., 101)\n",
    "e1[0] += 1e-5\n",
    "X = domain(e1, e2, 0., squeeze_out=True)\n",
    "\n",
    "print(X[0].shape)\n",
    "\n",
    "im1 = ax1.contourf(X[0], X[2], mhd_equil.absB0(e1, e2, 0., squeeze_out=True), levels=51)\n",
    "ax1.axis('equal')\n",
    "ax1.set_title('abs(B) at $\\phi=0$')\n",
    "fig.colorbar(im1)\n",
    "\n",
    "im2 = ax2.contourf(X[0], X[2], mhd_equil.absB0(e1, e2, 0., squeeze_out=True), levels=51)\n",
    "ax2.axis('equal')\n",
    "ax2.set_title('abs(B) at $\\phi=0$')\n",
    "fig.colorbar(im2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need to build the grid; the grid info can be obtained as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import h5py\n",
    "\n",
    "file = h5py.File(os.path.join(path_out, 'data/', 'data_proc0.hdf5'), 'r')\n",
    "grid_info = file['scalar'].attrs['grid_info']\n",
    "file.close()\n",
    "\n",
    "grid_info"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now add the grid lines, as well as the plasma vessel wall (limiter):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "e1 = np.linspace(grid_info[0, 0], grid_info[0, 1],\n",
    "                    int(grid_info[0, 2]) + 1)\n",
    "e2 = np.linspace(grid_info[0, 3], grid_info[1, 4],\n",
    "                    int(grid_info[0, 5]) + 1)\n",
    "X = domain(e1, e2, 0.)\n",
    "\n",
    "# plot xz-plane for torus mappings, xy-plane else\n",
    "if 'Torus' in domain_name or domain_name == 'GVECunit' or domain_name == 'Tokamak':\n",
    "    co1, co2 = 0, 2\n",
    "else:\n",
    "    co1, co2 = 0, 1\n",
    "\n",
    "# eta1-isolines\n",
    "for j in range(e1.size):\n",
    "    if j == 0:\n",
    "        pass\n",
    "    elif j == e1.size - 1:\n",
    "        ax1.plot(X[co1, j, :], X[co2, j, :], color='k')\n",
    "        ax2.plot(X[co1, j, :], X[co2, j, :], color='k')\n",
    "    else:\n",
    "        ax1.plot(X[co1, j, :], X[co2, j, :], color='tab:blue', alpha=.25)\n",
    "        ax2.plot(X[co1, j, :], X[co2, j, :], color='tab:blue', alpha=.25)\n",
    "\n",
    "# # eta2-isolines\n",
    "for k in range(e2.size):\n",
    "    if k == 0:\n",
    "        pass\n",
    "    else:\n",
    "        ax1.plot(X[co1, :, k], X[co2, :, k], color='tab:blue', alpha=.25)\n",
    "        ax2.plot(X[co1, :, k], X[co2, :, k], color='tab:blue', alpha=.25)\n",
    "\n",
    "ax1.plot(mhd_equil.limiter_pts_R, mhd_equil.limiter_pts_Z, 'tab:orange')\n",
    "ax2.plot(mhd_equil.limiter_pts_R, mhd_equil.limiter_pts_Z, 'tab:orange')\n",
    "\n",
    "ax1.set_xlabel('R [m]')\n",
    "ax1.set_ylabel('Z [m]')\n",
    "ax1.axis('equal')\n",
    "ax1.set_title('abs(B) at $\\phi=0$ with grid and limiter')\n",
    "\n",
    "ax2.set_xlabel('R [m]')\n",
    "ax2.set_ylabel('Z [m]')\n",
    "ax2.axis('equal')\n",
    "ax2.set_title('abs(B) at $\\phi=0$ with grid and limiter')\n",
    "\n",
    "fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we add the particle orbits:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(pos.shape[1]):\n",
    "    ax1.scatter(pos[:, i, 0], pos[:, i, 2], s=1)\n",
    "   \n",
    "ax1.set_title('Passing and trapped particles (co- and counter direction)')\n",
    "    \n",
    "fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We shall now add the [DriftKinetic](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.toy.DriftKinetic) particles:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_out = os.path.join(struphy.__path__[0], 'io/out/', 'tutorial_05b/')\n",
    "\n",
    "os.listdir(path_out)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us get some meta data on the simulations used in this notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(os.path.join(path_out, 'meta.txt')) as file:\n",
    "    print(file.read())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us look at the parameter file of the simulation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params_path = os.path.join(struphy.__path__[0], 'io/inp', 'tutorials', 'params_05b.yml')\n",
    "\n",
    "import yaml\n",
    "\n",
    "with open(params_path) as file:\n",
    "    parameters = yaml.load(file, Loader=yaml.FullLoader)\n",
    "    \n",
    "parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Under the key word `kinetic` we see that only 4 particles were simulated, each one with carefully chosen initial conditions (`markers/loading/initial`).\n",
    "\n",
    "The raw data has already been post-processed (presence of the folder `post_processing/`). Let us look at the resulting data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.listdir(os.path.join(path_out, 'post_processing'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us start by loading the time grid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "tgrid = np.load(os.path.join(path_out, 'post_processing', 't_grid.npy'))\n",
    "Nt = len(tgrid) - 1\n",
    "log_Nt = int(np.log10(Nt)) + 1\n",
    "\n",
    "print(Nt, log_Nt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.listdir(os.path.join(path_out, 'post_processing', 'kinetic_data'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.listdir(os.path.join(path_out, 'post_processing', 'kinetic_data', 'ions'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from tqdm import tqdm\n",
    "\n",
    "Np = parameters['kinetic']['ions']['save_data']['n_markers']\n",
    "    \n",
    "pos = np.zeros((Nt + 1, Np, 3), dtype=float)\n",
    "\n",
    "for n in tqdm(range(Nt + 1)):\n",
    "\n",
    "    # load x, y, z coordinates\n",
    "    orbits_path = os.path.join(path_out, 'post_processing', 'kinetic_data', 'ions', 'orbits', 'ions_{0:0{1}d}.txt'.format(n, log_Nt))\n",
    "    pos[n] = np.loadtxt(orbits_path, delimiter=',')[:, 1:]\n",
    "\n",
    "    # convert to R, y, z, coordinates\n",
    "    pos[n, :, 0] = np.sqrt(pos[n, :, 0]**2 + pos[n, :, 1]**2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us add the `DriftKinetic` orbits:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(pos.shape[1]):\n",
    "    ax2.scatter(pos[:, i, 0], pos[:, i, 2], s=1)\n",
    "   \n",
    "ax2.set_title('DriftKinetic version')\n",
    "    \n",
    "fig"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
