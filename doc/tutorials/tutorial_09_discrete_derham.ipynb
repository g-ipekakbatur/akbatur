{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 9 - The discrete de Rham sequence\n",
    "\n",
    "In this tutorial we will learn about the discrete FE spaces and the use of operators in the deRham diagram (below). The involved data structures have been discussed in [Tutorial 08 - Struphy data structures](https://struphy.pages.mpcdf.de/struphy/tutorials/tutorial_08_data_structures.html?highlight=data%20structures).\n",
    "\n",
    "The basics of the 3d DeRham diagram are explained in the [struphy documentation](https://struphy.pages.mpcdf.de/struphy/sections/discretization.html#geometric-finite-elements-feec).\n",
    "\n",
    "![hi](../pics/derham_complex.png)\n",
    "\n",
    "The discrete complex in the above diagram (lower row) is loaded via the **Derham** class: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mpi4py import MPI\n",
    "from struphy.feec.psydac_derham import Derham\n",
    "\n",
    "Nel = [9, 9, 10]  # Number of grid cells\n",
    "p = [1, 2, 3]  # spline degrees\n",
    "spl_kind = [False, True, True]   # spline types (clamped vs. periodic)\n",
    "\n",
    "comm = MPI.COMM_WORLD\n",
    "derham = Derham(Nel, p, spl_kind, comm=comm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us inspect the properties of `Derham`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.tutorials.utilities import print_all_attr\n",
    "\n",
    "print_all_attr(derham)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Callable FE fields\n",
    "\n",
    "Let us create an element of each discrete space $V_h^n$ and assign a name to it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p0 = derham.create_field('pressure', 'H1')\n",
    "e1 = derham.create_field('e_field', 'Hcurl')\n",
    "b2 = derham.create_field('b_field', 'Hdiv')\n",
    "n3 = derham.create_field('density', 'L2')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Moreover, let us initialize these fields in the following way:\n",
    "\n",
    "* The `pressure` and the 1st component of `e_field` are sinusoidal functions of mode number 2 and amplitude 0.5 in the third direction\n",
    "* The 3rd component of `e_field` and the 2nd component of `b_field` are superpositions of two cosines with mode numbers 1 and 2 and amplitudes 0.75 and 0.5, , respectively, in the second direction\n",
    "* The `density` has noise of amplitude $10^{-3}$ in the third direction\n",
    "* all other components are zero"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.geometry import domains\n",
    "\n",
    "domain = domains.Cuboid()\n",
    "\n",
    "init_params = {'type': ['ModesSin', 'ModesCos', 'noise'],\n",
    "               'ModesSin': {'comps': {'pressure': '0',\n",
    "                                      'e_field': ['v', None, None]},\n",
    "                            'ls': {'pressure': [0],\n",
    "                                   'e_field': [[0], None, None]},\n",
    "                            'ms': {'pressure': [0],\n",
    "                                   'e_field': [[0], None, None]},\n",
    "                            'ns': {'pressure': [2],\n",
    "                                   'e_field': [[2], None, None]},\n",
    "                            'amps': {'pressure': [.5],\n",
    "                                     'e_field': [[.5], None, None]},\n",
    "                            },\n",
    "               'ModesCos': {'comps': {'e_field': [None, None, 'v'],\n",
    "                                      'b_field': [None, 'v', None]},\n",
    "                            'ls': {'e_field': [None, None, [0, 0]],\n",
    "                                   'b_field': [None, [0, 0], None]},\n",
    "                            'ms': {'e_field': [None, None, [1, 2]],\n",
    "                                   'b_field': [None, [1, 2], None]},\n",
    "                            'ns': {'e_field': [None, None, [0, 0]],\n",
    "                                   'b_field': [None, [0, 0], None]},\n",
    "                            'amps': {'e_field': [None, None, [.75, .5]],\n",
    "                                     'b_field': [None,[.75, .5], None]},\n",
    "                            },\n",
    "               'noise': {'comps': {'density': [True]},\n",
    "                        'variation_in': 'e3',\n",
    "                        'amp': 0.001,\n",
    "                        'seed': 3456546}}\n",
    "\n",
    "p0.initialize_coeffs(init_params, domain)\n",
    "e1.initialize_coeffs(init_params, domain)\n",
    "b2.initialize_coeffs(init_params, domain)\n",
    "n3.initialize_coeffs(init_params, domain)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us evaluate these fields, squeeze the output and plot all components for verification:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "# evaluation points\n",
    "eta1 = 0\n",
    "eta2 = np.linspace(0., 1., 50)\n",
    "eta3 = np.linspace(0., 1., 70)\n",
    "\n",
    "# evaluate 0-form\n",
    "p0_vals = p0(eta1, eta2, eta3, squeeze_output=True)\n",
    "print(f'{type(p0_vals) = }, {p0_vals.shape = }')\n",
    "\n",
    "# evaluate 1-form\n",
    "e1_vals = e1(eta1, eta2, eta3, squeeze_output=True)\n",
    "print(f'{type(e1_vals) = }, {type(e1_vals[0]) = }, {e1_vals[0].shape = }')\n",
    "\n",
    "# evaluate 2-form\n",
    "b2_vals = b2(eta1, eta2, eta3, squeeze_output=True)\n",
    "print(f'{type(b2_vals) = }, {type(b2_vals[0]) = }, {b2_vals[0].shape = }')\n",
    "\n",
    "# evaluate 3-form\n",
    "n3_vals = n3(eta1, eta2, eta3, squeeze_output=True)\n",
    "print(f'{type(n3_vals) = }, {n3_vals.shape = }')\n",
    "\n",
    "# plotting\n",
    "plt.figure(figsize=(12, 14))\n",
    "plt.subplot(4, 3, 1)\n",
    "plt.plot(eta3, p0_vals[0, :], label=p0.name)\n",
    "plt.xlabel('$\\eta_3$')\n",
    "plt.legend()\n",
    "\n",
    "plt.subplot(4, 3, 4)\n",
    "plt.plot(eta3, e1_vals[0][0, :], label=(e1.name + '_1'))\n",
    "plt.xlabel('$\\eta_3$')\n",
    "plt.legend()\n",
    "plt.subplot(4, 3, 5)\n",
    "plt.plot(eta3, e1_vals[1][0, :], label=(e1.name + '_2'))\n",
    "plt.xlabel('$\\eta_3$')\n",
    "plt.legend()\n",
    "plt.subplot(4, 3, 6)\n",
    "plt.plot(eta2, e1_vals[2][:, 0], label=(e1.name + '_3'))\n",
    "plt.xlabel('$\\eta_2$')\n",
    "plt.legend()\n",
    "\n",
    "plt.subplot(4, 3, 7)\n",
    "plt.plot(eta2, b2_vals[0][:, 0], label=(b2.name + '_1'))\n",
    "plt.xlabel('$\\eta_2$')\n",
    "plt.legend()\n",
    "plt.subplot(4, 3, 8)\n",
    "plt.plot(eta2, b2_vals[1][:, 0], label=(b2.name + '_2'))\n",
    "plt.xlabel('$\\eta_2$')\n",
    "plt.legend()\n",
    "plt.subplot(4, 3, 9)\n",
    "plt.plot(eta2, b2_vals[2][:, 0], label=(b2.name + '_3'))\n",
    "plt.xlabel('$\\eta_2$')\n",
    "plt.legend()\n",
    "\n",
    "plt.subplot(4, 3, 10)\n",
    "plt.plot(eta3, n3_vals[0, :], label=n3.name)\n",
    "plt.xlabel('$\\eta_3$')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Commuting projectors\n",
    "\n",
    "Next, we shall project a sinusoidal function into $V_h^0$ and, moreover, project its gradient into $V_h^1$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fun(x, y, z): return .5*np.sin(2*2*np.pi*z)\n",
    "\n",
    "fun_h = derham.P['0'](fun)\n",
    "print(f'{type(fun_h) = }')\n",
    "\n",
    "def dx_fun(x, y, z): return 0*z\n",
    "def dy_fun(x, y, z): return 0*z\n",
    "def dz_fun(x, y, z): return 2*2*np.pi*.5*np.cos(2*2*np.pi*z)\n",
    "\n",
    "dfun_h = derham.P['1']((dx_fun, dy_fun, dz_fun))\n",
    "print(f'{type(dfun_h) = }')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can check the commuting property by applying the discrete gradient operator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'{type(derham.grad) = }')\n",
    "gradfun_h = derham.grad.dot(fun_h)\n",
    "print(f'{type(gradfun_h) = }')\n",
    "\n",
    "assert np.allclose(dfun_h[0].toarray(), gradfun_h[0].toarray())\n",
    "assert np.allclose(dfun_h[1].toarray(), gradfun_h[1].toarray())\n",
    "assert np.allclose(dfun_h[2].toarray(), gradfun_h[2].toarray())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**All these operations also work in parallel!**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
