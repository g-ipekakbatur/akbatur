{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3 - Plotting fields in Tokamak geometry\n",
    "\n",
    "In this tutorial we will learn how to plot FEEC data in poloidal planes of a Tokamak geometry. \n",
    "\n",
    "The Struphy run generating the data for this tutorial can be launched via:\n",
    "\n",
    "    $ struphy test tutorials -n 3\n",
    "    \n",
    "All data generated in this way is stored in the Struphy installation path (obtained from ``struphy -p``) under ``io/out/tutorial_03``.\n",
    "\n",
    "## Time, logical and physical grids\n",
    "\n",
    "Let us start by inspecting the content of the `post_processing/` folder, see [Tutorial 2](https://struphy.pages.mpcdf.de/struphy/doc/_build/html/tutorials/tutorial_02_postproc_standard_plotting.html):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import struphy\n",
    "\n",
    "path_out = os.path.join(struphy.__path__[0], 'io/out/', 'tutorial_03/')\n",
    "data_path = os.path.join(path_out, 'post_processing')\n",
    "\n",
    "os.listdir(data_path)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since this was a run of the model `LinearMHD`, only `fields_data/` is present in the output. Let us get the time grid,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "t_grid = np.load(os.path.join(data_path, 't_grid.npy'))\n",
    "t_grid"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and inspect the `fields_data/`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fluid_path = os.path.join(data_path, 'fields_data')\n",
    "\n",
    "print(os.listdir(fluid_path))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The MHD variables (i.e. their values at predefined grid points) have been saved in the folder `mhd/` (note that the magnetic field is stored under `em_fields/`). \n",
    "\n",
    "Before plotting we want some information on the geometry, initial conditions etc. For this, let us load the parameter file of the simulation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params_path = os.path.join(path_out, 'parameters.yml')\n",
    "\n",
    "import yaml\n",
    "\n",
    "with open(params_path) as file:\n",
    "    parameters = yaml.load(file, Loader=yaml.FullLoader)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us look at the grid, geometry and fluid species:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['grid']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['geometry']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['fluid']"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The simulation was done in `Tokamak` geometry with a resolution of `[8, 32, 4]`. In any toroidal geometry in Struphy, poloidal planes are always parametrized by the first two coordinates $(\\eta_1, \\eta_2)$ (here with a resolution `[8, 32]`), where $\\eta_1$ parametrizes the radial coordinate and $\\eta_2$ the angle coordinate. The toroidal angle is always parametrized by $\\eta_3$ (here with a resolution of `[4]`). \n",
    "\n",
    "Moreover, we spot an MHD initial condition of type `TorusModesSin`, where the second (poloidal-angle) component of the MHD velocity `u2` is initialized in logical coordinates with a poloidal mode number $m=3$, toroidal mode number $n=1$ and an amlitude 0.001. The radial profile is given by a half-period `sin` function (zero at the edges).\n",
    "\n",
    "Let us first load the logical 1d grid vectors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pickle\n",
    "\n",
    "with open(os.path.join(fluid_path, 'grids_log.bin'), 'rb') as file:\n",
    "    e1_grid, e2_grid, e3_grid = pickle.load(file)\n",
    "    \n",
    "print('Shape of the e1_grid:', e1_grid.shape)\n",
    "print('Shape of the e2_grid:', e2_grid.shape)\n",
    "print('Shape of the e3_grid:', e3_grid.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The **logical grid** is a product grid. In any one poloidal plane, the grid is a product of the `e1_grid` and `e2_grid` vectors: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt\n",
    "\n",
    "ee1, ee2 = np.meshgrid(e1_grid, e2_grid, indexing='ij')\n",
    "\n",
    "for i in range(e1_grid.size):\n",
    "    plt.plot(ee1[i, :], ee2[i, :], 'tab:blue', alpha=.5, zorder=0)\n",
    "for j in range(e2_grid.size):\n",
    "    plt.plot(ee1[:, j], ee2[:, j], 'tab:blue', alpha=.5, zorder=0)\n",
    "plt.xlabel('$\\eta_1$')\n",
    "plt.ylabel('$\\eta_2$')\n",
    "plt.title(f'logical poloidal grid')\n",
    "plt.axis('equal')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now load the **physical grid** (mapped from the logical grid):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(os.path.join(fluid_path, 'grids_phy.bin'), 'rb') as file:\n",
    "    x_grid, y_grid, z_grid = pickle.load(file)\n",
    "    \n",
    "print('Shape of the x-grid:', x_grid.shape)\n",
    "print('Shape of the y-grid:', y_grid.shape)\n",
    "print('Shape of the z-grid:', z_grid.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The physical grids are already stored as meshgrids, because they are not product grids.\n",
    "\n",
    "In a Tokamak, we need to compute the $R$- and $\\phi$-grids from the physical grid (please check out the [Struphy domain documentation](https://struphy.pages.mpcdf.de/struphy/sections/domains.html) for more details on the definition of the specific mappings). In Struphy, we have the convention that the **poloidal angle goes counter-clock-wise** (at toroidal $\\phi = 0$) and the **toroidal angle goes clock-wise** when viewed from the top. \n",
    "\n",
    "The mapping $F_2: (R, \\phi, Z) \\mapsto (x, y, z)$ is thus given by\n",
    "$$\n",
    "\\begin{aligned}\n",
    " x &= R \\cos(\\phi) \\\\\n",
    " y &= -R\\sin(\\phi) \\\\\n",
    " z &= Z\n",
    " \\end{aligned}\n",
    "$$\n",
    "\n",
    "The Jacobian and its inverse are given by\n",
    "$$\n",
    " DF_2 = \\begin{pmatrix}\n",
    " \\cos(\\phi) & - R \\sin(\\phi) & 0\n",
    " \\\\\n",
    " -\\sin(\\phi) & - R \\cos(\\phi) & 0\n",
    " \\\\\n",
    " 0 & 0 & 1\n",
    " \\end{pmatrix}\\,,\\qquad\n",
    " DF_2^{-1} = \\begin{pmatrix}\n",
    " \\cos(\\phi) & - \\sin(\\phi) & 0\n",
    " \\\\\n",
    " -\\frac 1R \\sin(\\phi) & - \\frac 1R \\cos(\\phi) & 0\n",
    " \\\\\n",
    " 0 & 0 & 1\n",
    " \\end{pmatrix}\\,.\n",
    "$$\n",
    "The Jacobian determinant and metric tensor read\n",
    "$$\n",
    " \\textnormal{det} DF_2 = - R\\,,\\qquad G = \\begin{pmatrix}\n",
    " 1 & 0 & 0\n",
    " \\\\\n",
    " 0 &  R^2 & 0\n",
    " \\\\\n",
    " 0 & 0 & 1\n",
    " \\end{pmatrix}\\,.\n",
    "$$\n",
    "\n",
    "\n",
    "We must pay attention to the parameter `tor_period` of the [Tokamak](https://struphy.pages.mpcdf.de/struphy/sections/domains.html#struphy.geometry.domains.Tokamak) geometry, which was set to 3, meaning that only one third of the total Torus has been simulated, $\\phi \\in (0, 2\\pi/3)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "R_grid = np.sqrt(x_grid**2 + y_grid**2)\n",
    "phi_grid = np.arctan2(-y_grid, x_grid)\n",
    "\n",
    "for n in range(phi_grid.shape[2]):\n",
    "    print(phi_grid[0, 0, n])\n",
    "    \n",
    "print('check: 2pi/3 = ', 2*np.pi/3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now plot the grids of the five available poloidal planes. Since the geometry was a [Tokamak](https://struphy.pages.mpcdf.de/struphy/sections/domains.html#struphy.geometry.domains.Tokamak), they must be identical:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt\n",
    "\n",
    "n_poloidal_planes = e3_grid.size\n",
    "\n",
    "plt.figure(figsize=(12, 16))\n",
    "for n in range(n_poloidal_planes):\n",
    "    plt.subplot(3, 2, n + 1)\n",
    "    for i in range(R_grid.shape[0]):\n",
    "        plt.plot(R_grid[i, :, n], z_grid[i, :, n], 'tab:blue', alpha=.5, zorder=0)\n",
    "    for j in range(R_grid.shape[1] - 1):\n",
    "        plt.plot(R_grid[:, j, n], z_grid[:, j, n], 'tab:blue', alpha=.5, zorder=0)\n",
    "    plt.xlabel('R')\n",
    "    plt.ylabel('Z')\n",
    "    plt.title(f'$\\phi = {phi_grid[0, 0, n]}$')\n",
    "    plt.axis('equal')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## MHD velocity plots\n",
    "\n",
    "In the simulation for this notebook, the MHD velocity was in the space $u \\in (H^1(\\Omega))^3$ representing vector-fields (contra-variant representation). \n",
    "\n",
    "We start by loaging the values of $u$ in logical space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(os.path.join(fluid_path, 'mhd/', 'u2_log.bin'), 'rb') as file:\n",
    "    u_log = pickle.load(file)\n",
    "\n",
    "for key, val in u_log.items():\n",
    "    print(key)\n",
    "    for va in val:\n",
    "        print(va.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot the initial condition of the second comonent of $u$ in the **logical poloidal planes** ($\\eta_1$-$\\eta_2$):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u_log_init = u_log[0.0]\n",
    "\n",
    "plt.figure(figsize=(12, 16))\n",
    "for n in range(n_poloidal_planes):\n",
    "    plt.subplot(3, 2, n + 1)\n",
    "    plt.contourf(e1_grid, e2_grid, u_log_init[1][:, :, n].T)\n",
    "    plt.xlabel('$\\eta_1$')\n",
    "    plt.ylabel('$\\eta_2$')\n",
    "    plt.title(f'$\\hat u_2(t=0)$ at $\\eta_3={e3_grid[n]}$')\n",
    "    plt.colorbar()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see the initial condition with a poloidal mode number $m=3$ (in $\\eta_2$), a toroidal mode number $n=1$ (in $\\eta_3$) and the half-sine function in the radial $\\eta_1$ direction. The amplitude of 0.001 also corresponds to the value specified in the parameter file.\n",
    "\n",
    "The component $\\hat u_2$ is the contra-variant component (because $u \\in (H^1(\\Omega))^3$) along the angle-direction $\\eta_2$, which is the poloidal angle of a torus. Let us plot the same initial conditions as above in the physical domain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(12, 16))\n",
    "for n in range(n_poloidal_planes):\n",
    "    plt.subplot(3, 2, n + 1)\n",
    "    plt.contourf(R_grid[:, :, n], z_grid[:, :, n], u_log_init[1][:, :, n])\n",
    "    plt.colorbar()\n",
    "    plt.title(f'$\\hat u_2(t=0)$ at $\\phi={phi_grid[0, 0, n]}$')\n",
    "    plt.xlabel('R')\n",
    "    plt.ylabel('Z')\n",
    "    plt.axis('equal')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Moreover, we can plot the vector components in different coordinates. For instance, let us plot $u^*_R$, i.e. the R-component of the vector field in $(R, \\phi,Z)$-cooridnates, denoted with a star.\n",
    "\n",
    " For this we need to pullback the physical $(x, y, z)$-components of $u$ to $(R, \\phi, Z)$-space. The pullback is given by the inverse Jacobian,\n",
    "$$\n",
    " u^* = DF_2^{-1} u\\,.\n",
    "$$\n",
    "\n",
    "For the first ($R$-)component this means\n",
    "$$\n",
    " u^*_R = \\cos(\\phi) u_x - \\sin(\\phi) u_y\\,.\n",
    "$$\n",
    "\n",
    "We now load the values of $u$ in the physical domain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(os.path.join(fluid_path, 'mhd/', 'u2_phy.bin'), 'rb') as file:\n",
    "    u_phy = pickle.load(file)\n",
    "\n",
    "for key, val in u_phy.items():\n",
    "    print(key)\n",
    "    for va in val:\n",
    "        print(va.shape)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot the initial condition of $u^*_R$ in the five poloidal planes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u_phy_init = u_phy[0.0]\n",
    "\n",
    "plt.figure(figsize=(12, 16))\n",
    "for n in range(n_poloidal_planes):\n",
    "    \n",
    "    uR = np.cos(phi_grid[:, :, n]) * u_phy_init[0][:, :, n] - np.sin(phi_grid[:, :, n]) * u_phy_init[1][:, :, n]\n",
    "    \n",
    "    plt.subplot(3, 2, n + 1)\n",
    "    plt.contourf(R_grid[:, :, n], z_grid[:, :, n], uR)\n",
    "    plt.xlabel('$R$')\n",
    "    plt.ylabel('$Z$')\n",
    "    plt.title(f'$u^*_R(t=0)$ at $\\phi={phi_grid[0, 0, n]}$')\n",
    "    plt.colorbar()\n",
    "    plt.axis('equal')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also show the vector field $(u^*_R, u^*_Z)$ in the poiloidal $(R,Z)$-plane:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(12, 16))\n",
    "for n in range(n_poloidal_planes):\n",
    "    \n",
    "    uR = np.cos(phi_grid[:, :, n]) * u_phy_init[0][:, :, n] - np.sin(phi_grid[:, :, n]) * u_phy_init[1][:, :, n]\n",
    "    uZ = u_phy_init[2][:, :, n]\n",
    "    \n",
    "    plt.subplot(3, 2, n + 1)\n",
    "    plt.contourf(R_grid[:, :, n], z_grid[:, :, n], np.sqrt(uR**2 + uZ**2))\n",
    "    plt.quiver(R_grid[:, :, n], z_grid[:, :, n], uR, uZ, scale=.03, color='w')\n",
    "    plt.xlabel('$R$')\n",
    "    plt.ylabel('$Z$')\n",
    "    plt.title(f'$u^*_R(t=0)$ at $\\phi={phi_grid[0, 0, n]}$')\n",
    "    plt.colorbar()\n",
    "    plt.axis('equal')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Time evolution \n",
    "\n",
    "Finally, let us plot the time evolution of the poloidal component $\\hat u_2$ on the physical domain, at $\\phi=0$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(12, 16))\n",
    "\n",
    "for n, (t, u_comps) in enumerate(u_log.items()): \n",
    "    plt.subplot(3, 2, n + 1)\n",
    "    plt.contourf(R_grid[:, :, 0], z_grid[:, :, 0], u_comps[1][:, :, 0])\n",
    "    plt.colorbar()\n",
    "    plt.title(f'$\\hat u_2(R, 0, Z)$ at $\\phi=0$ at t={t}')\n",
    "    plt.xlabel('R')\n",
    "    plt.ylabel('Z')\n",
    "    plt.axis('equal')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As mentioned before, the velocity is here represented in the space $(H^1(\\Omega))^3$, which in Struphy is the space for \"vector fields\" (=contra-variant components). We can also plot the co-variant components (1-form) of the velocity. \n",
    "\n",
    "For this, we need to recreate the domain used in the simulation and transform vector-field to 1-form components with  the `domain.transform` method: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.geometry.domains import Tokamak\n",
    "\n",
    "params_map = parameters['geometry']['Tokamak']\n",
    "\n",
    "domain = Tokamak(**params_map)\n",
    "\n",
    "plt.figure(figsize=(12, 16))\n",
    "\n",
    "for n, (t, u_comps) in enumerate(u_log.items()): \n",
    "    \n",
    "    u1 = domain.transform(u_comps, e1_grid, e2_grid, e3_grid, kind='v_to_1')\n",
    "    \n",
    "    plt.subplot(3, 2, n + 1)\n",
    "    plt.contourf(R_grid[:, :, 0], z_grid[:, :, 0], u1[1][:, :, 0])\n",
    "    plt.colorbar()\n",
    "    plt.title(f'1-form comp. $\\hat u^1_2(R, 0, Z)$ at t={t}')\n",
    "    plt.xlabel('R')\n",
    "    plt.ylabel('Z')\n",
    "    plt.axis('equal')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
