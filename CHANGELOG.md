## Version 2.1.1

Small bug fix in `StencilMatrix`: the correct values of flags `backend` and `precompiled` need to be passed to its methods. This has been fixed in psydac 0.1.7 which is now installed by default.