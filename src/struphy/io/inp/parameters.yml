# Some hints on how to use this parameter file:

# 1. Strings can either be set as e.g. 'Cuboid' or Cuboid, i.e. with or without quotes - both works
# 2. The parameter null will be transformed to Python's None type
# 3. Available geometries: https://struphy.pages.mpcdf.de/struphy/sections/domains.html
# 4. Available MHD equilibria: https://struphy.pages.mpcdf.de/struphy/sections/mhd_equils.html
# 5. Available kinetic backgrounds/initial conditions: https://struphy.pages.mpcdf.de/struphy/sections/kinetic_backgrounds.html
# 6. Available FEEC initial conditions: https://struphy.pages.mpcdf.de/struphy/sections/inits.html

grid :
    Nel          : [12, 14, 1] # number of grid cells, >=p
    p            : [2, 3, 1]  # spline degree
    spl_kind     : [False, True, True] # spline type: True=periodic, False=clamped
    dirichlet_bc : null # [[False, False], [False, False], [False, False]], hom. Dirichlet boundary conditions for N-splines (spl_kind must be False)
    dims_mask    : [True, True, True] # True if the dimension is to be used in the mpi domain decomposition (=default for each dimension).
    nq_el        : [2, 2, 1] # quadrature points per grid cell
    nq_pr        : [2, 2, 1] # quadrature points per histopolation cell (for commuting projectors)
    polar_ck     : -1 # C^k smoothness at polar singularity at eta_1=0 (default: -1 --> standard tensor product, 1 : polar splines)

time :
    dt         : 0.005 # time step
    Tend       : 0.015 # simulation time interval is [0, Tend]
    split_algo : LieTrotter # LieTrotter | Strang

units : # units not stated here can be viewed via "struphy units -h"
    x : 1. # length scale unit in m
    B : 1. # magnetic field unit in T
    n : 1. # number density unit in 10^20 m^(-3)

geometry :
    type : Cuboid 
    Cuboid : {}

mhd_equilibrium :
    type : HomogenSlab 
    HomogenSlab : {}

electric_equilibrium :
    type : HomogenSlab # (possible choices seen below)
    HomogenSlab :
        phi0  : 1. # constant electric potential

em_fields :
    init :
        type : TorusModesCos
        TorusModesCos :
            comps :
                e1 : [null, 'v', null] # space of components to be initialized (for scalar fields: no list)
                b2 : [null, 'v', null]    # space of components to be initialized (for scalar fields: no list)
            ms :
                e1 : [null, [3], null]  # poloidal mode numbers
                b2 : [null, [3], null] # poloidal mode numbers
            ns :
                e1 : [null, [1], null] # toroidal mode numbers
                b2 : [null, [1], null] # toroidal mode numbers
            amps :
                e1 : [null, [0.001], null] # amplitudes of each mode
                b2 : [null, [0.001], null] # amplitudes of each mode
            pfuns :
                e1 : [null, ['sin'], null] # profile function in eta1-direction ('sin' or 'cos' or 'exp' or 'd_exp')
                b2 : [null, ['sin'], null] # profile function in eta1-direction ('sin' or 'cos' or 'exp' or 'd_exp')
            pfun_params :
                e1 : [null, null, null] # Provides [r_0, sigma] parameters for each "exp" or "d_exp" profile fucntion, and null for "sin" and "cos"
                b2 : [null, null, null] # Provides [r_0, sigma] parameters for each "exp" or "d_exp" profile fucntion, and null for "sin" and "cos"

fluid :
    mhd :
        phys_params:
            A : 1 # mass number in units of proton mass
            Z : 1 # signed charge number in units of elementary charge
        mhd_u_space : H1vec # Hdiv | H1vec
        init :
            type : TorusModesCos
            TorusModesCos :
                comps :
                    n3 : null                     # space of components to be initialized (for scalar fields: no list)
                    u2 : [null, 'v', null] # space of components to be initialized (for scalar fields: no list)
                    p3 : '0'                     # space of components to be initialized (for scalar fields: no list)
                ms :
                    n3 : null              # poloidal mode numbers
                    u2 : [null, [3], null] # poloidal mode numbers
                    p3 : [3]              # poloidal mode numbers
                ns :
                    n3 : null              # toroidal mode numbers
                    u2 : [null, [1], null] # toroidal mode numbers
                    p3 : [1]              # toroidal mode numbers
                amps :
                    n3 : null                   # amplitude of each mode
                    u2 : [null, [0.001], null] # amplitude of each mode
                    p3 : [0.001]                   # amplitude of each mode
                pfuns :
                    n3 : null                  # profile function in eta1-direction ('sin' or 'cos' or 'exp' or 'd_exp')
                    u2 : [null, ['sin'], null] # profile function in eta1-direction ('sin' or 'cos' or 'exp' or 'd_exp')
                    p3 : ['sin']                  # profile function in eta1-direction ('sin' or 'cos' or 'exp' or 'd_exp')        
                pfun_params :
                    n3 : null               # Provides [r_0, sigma] parameters for each "exp" and "d_exp" profile fucntion, and null for "sin" and "cos"
                    u2 : [null, null, null] # Provides [r_0, sigma] parameters for each "exp" and "d_exp" profile fucntion, and null for "sin" and "cos"
                    p3 : null               # Provides [r_0, sigma] parameters for each "exp" and "d_exp" profile fucntion, and null for "sin" and "cos"        

kinetic :
    ions :
        phys_params :
            A : 1  # mass number in units of proton mass
            Z : 1 # signed charge number in units of elementary charge
        markers :
            type    : full_f # full_f, control_variate, or delta_f
            ppc     : 10  # number of markers per 3d grid cell
            Np      : 3 # alternative if ppc = null (total number of markers, must be larger or equal than # MPI processes)
            eps     : .25 # MPI send/receive buffer (0.1 <= eps <= 1.0)
            bc : 
                type    : [periodic, periodic, periodic] # marker boundary conditions: remove, reflect or periodic
            loading :
                type          : pseudo_random # particle loading mechanism 
                seed          : 1234 # seed for random number generator
                moments       : [0., 0., 0., 1., 1., 1.] # moments of Gaussian s3, see background/moms_spec
                spatial       : uniform # uniform or disc
                dir_particles : 'path_to_particles' # directory of particles if loaded externally
        init :
            type : Maxwellian6DPerturbed
            Maxwellian6DPerturbed :
                n :
                    n0 : 0.05
                    perturbation :
                        l : [0]
                        m : [0]
                        n : [0]
                        amps_sin : [0.]
                        amps_cos : [0.]
                u1 :
                    u10 : 0.
                u2 :
                    u20 : 2.5
                u3 :
                    u30 : 0.
                vth1 :
                    vth10 : 1.
                vth2 :
                    vth20 : 1.
                vth3 :
                    vth30 : 1.
        background :
            type : Maxwellian6DUniform
            Maxwellian6DUniform :
                n  : 1.
                u1 : 0.
                u2 : 0.
                u3 : 0.
                vth1 : 1.
                vth2 : 1.
                vth3 : 1.
        save_data :
            n_markers : 3 # number of markers to be saved during simulation
            f :
                slices : [v1] # in which directions to bin (e.g. [e1_e2, v1_v2_v3])
                n_bins : [[32]] # number of bins in each direction (e.g. [[16, 20], [16, 18, 22]])
                ranges : [[[-3., 3.]]] # bin range in each direction (e.g. [[[0., 1.], [0., 1.]], [[-3., 3.], [-4., 4.], [-5., 5.]]])