import pytest


@pytest.mark.mpi(min_size=2)
@pytest.mark.parametrize('Nel', [[64, 1, 1], [1, 64, 1], [1, 1, 64], [63, 64, 1], [64, 1, 63], [1, 63, 64]])
@pytest.mark.parametrize('p', [[1, 1, 1]])
@pytest.mark.parametrize('spl_kind', [[True, True, True]])
def test_lowdim_derham(Nel, p, spl_kind, do_plot=False):
    '''Test Nel=1 in various directions.'''

    from mpi4py import MPI
    import numpy as np
    from matplotlib import pyplot as plt

    from struphy.feec.psydac_derham import Derham

    from psydac.linalg.stencil import StencilVector
    from psydac.linalg.block import BlockVector

    comm = MPI.COMM_WORLD
    assert comm.size >= 2
    rank = comm.Get_rank()

    print('Nel=', Nel)
    print('p=', p)
    print('spl_kind=', spl_kind)

    # Psydac discrete Derham sequence
    derham = Derham(Nel, p, spl_kind, comm=comm)

    ############################
    ### TEST STENCIL VECTORS ###
    ############################
    # Stencil vectors for Psydac:
    x0_PSY = StencilVector(derham.Vh['0'])
    print(f'rank {rank} | 0-form StencilVector:')
    print(f'rank {rank} | starts:', x0_PSY.starts)
    print(f'rank {rank} | ends  :', x0_PSY.ends)
    print(f'rank {rank} | pads  :', x0_PSY.pads)
    print(f'rank {rank} | shape (=dim):', x0_PSY.shape)
    print(f'rank {rank} | [:].shape (=shape):', x0_PSY[:].shape)

    x3_PSY = StencilVector(derham.Vh['3'])
    print(f'rank {rank} | \n3-form StencilVector:')
    print(f'rank {rank} | starts:', x3_PSY.starts)
    print(f'rank {rank} | ends  :', x3_PSY.ends)
    print(f'rank {rank} | pads  :', x3_PSY.pads)
    print(f'rank {rank} | shape (=dim):', x3_PSY.shape)
    print(f'rank {rank} | [:].shape (=shape):', x3_PSY[:].shape)

    # Block of StencilVecttors
    x1_PSY = BlockVector(derham.Vh['1'])
    print(f'rank {rank} | \n1-form StencilVector:')
    print(f'rank {rank} | starts:', [component.starts for component in x1_PSY])
    print(f'rank {rank} | ends  :', [component.ends for component in x1_PSY])
    print(f'rank {rank} | pads  :', [component.pads for component in x1_PSY])
    print(f'rank {rank} | shape (=dim):', [
          component.shape for component in x1_PSY])
    print(f'rank {rank} | [:].shape (=shape):', [
          component[:].shape for component in x1_PSY])

    x2_PSY = BlockVector(derham.Vh['2'])
    print(f'rank {rank} | \n2-form StencilVector:')
    print(f'rank {rank} | starts:', [component.starts for component in x2_PSY])
    print(f'rank {rank} | ends  :', [component.ends for component in x2_PSY])
    print(f'rank {rank} | pads  :', [component.pads for component in x2_PSY])
    print(f'rank {rank} | shape (=dim):', [
          component.shape for component in x2_PSY])
    print(f'rank {rank} | [:].shape (=shape):', [
          component[:].shape for component in x2_PSY])

    xv_PSY = BlockVector(derham.Vh['v'])
    print(f'rank {rank} | \nVector StencilVector:')
    print(f'rank {rank} | starts:', [component.starts for component in xv_PSY])
    print(f'rank {rank} | ends  :', [component.ends for component in xv_PSY])
    print(f'rank {rank} | pads  :', [component.pads for component in xv_PSY])
    print(f'rank {rank} | shape (=dim):', [
          component.shape for component in xv_PSY])
    print(f'rank {rank} | [:].shape (=shape):', [
          component[:].shape for component in xv_PSY])

    #################################
    ### TEST COMMUTING PROJECTORS ###
    #################################
    def fun(eta): return np.cos(2*np.pi*eta)
    def dfun(eta): return - 2*np.pi * np.sin(2*np.pi*eta)
    
    # evaluation points and gradient
    e1 = 0.
    e2 = 0.
    e3 = 0.
    if Nel[0] > 1:
        e1 = np.linspace(0., 1., 100)
        e = e1
        c = 0
        def f(x, y, z): return fun(x)
        def dfx(x, y, z): return dfun(x)
        def dfy(x, y, z): return np.zeros_like(x)
        def dfz(x, y, z): return np.zeros_like(x)
    elif Nel[1] > 1:
        e2 = np.linspace(0., 1., 100)
        e = e2
        c = 1
        def f(x, y, z): return fun(y)
        def dfx(x, y, z): return np.zeros_like(y)
        def dfy(x, y, z): return dfun(y)
        def dfz(x, y, z): return np.zeros_like(y)
    elif Nel[2] > 1:
        e3 = np.linspace(0., 1., 100)
        e = e3
        c = 2
        def f(x, y, z): return fun(z)
        def dfx(x, y, z): return np.zeros_like(z)
        def dfy(x, y, z): return np.zeros_like(z)
        def dfz(x, y, z): return dfun(z)
    
    def curl_f_1(x, y, z): return dfy(x, y, z) - dfz(x, y, z)
    def curl_f_2(x, y, z): return dfz(x, y, z) - dfx(x, y, z)
    def curl_f_3(x, y, z): return dfx(x, y, z) - dfy(x, y, z)
    
    def div_f(x, y, z): return dfx(x, y, z) + dfy(x, y, z) + dfz(x, y, z)
    
    grad_f = (dfx, dfy, dfz)
    curl_f = (curl_f_1, curl_f_2, curl_f_3)
    proj_of_grad_f = derham.P['1'](grad_f)
    proj_of_curl_fff = derham.P['2'](curl_f)
    proj_of_div_fff = derham.P['3'](div_f)

    ##########
    # 0-form #
    ##########
    f0_h = derham.P['0'](f)

    field_f0 = derham.create_field('f0', 'H1')
    field_f0.vector = f0_h
    field_f0_vals = field_f0(e1, e2, e3, squeeze_output=True)

    # a) projection error
    err_f0 = np.max(np.abs(f(e1, e2, e3) - field_f0_vals))
    print(f'\n{err_f0 = }')
    assert err_f0 < 1e-2

    # b) commuting property
    df0_h = derham.grad.dot(f0_h)
    assert np.allclose(df0_h.toarray(), proj_of_grad_f.toarray())

    # c) derivative error
    field_df0 = derham.create_field('df0', 'Hcurl')
    field_df0.vector = df0_h
    field_df0_vals = field_df0(e1, e2, e3, squeeze_output=True)

    err_df0 = [np.max(np.abs(exact(e1, e2, e3) - field_v)) for exact, field_v in zip(grad_f, field_df0_vals)]
    print(f'{err_df0 = }')
    assert np.max(err_df0) < .4

    # d) plotting
    plt.figure(figsize=(8, 12))
    plt.subplot(2, 1, 1)
    plt.plot(e, f(e1, e2, e3), 'o')
    plt.plot(e, field_f0_vals)
    plt.title('fun')
    plt.xlabel(f'eta{c + 1}')

    plt.subplot(2, 1, 2)
    plt.plot(e, grad_f[c](e1, e2, e3), 'o')
    plt.plot(e, field_df0_vals[c])
    plt.title(f'grad comp {c + 1}')
    
    plt.subplots_adjust(wspace=1.0, hspace=0.4)

    ##########
    # 1-form #
    ##########
    f1_h = derham.P['1']((f, f, f))

    field_f1 = derham.create_field('f1', 'Hcurl')
    field_f1.vector = f1_h
    field_f1_vals = field_f1(e1, e2, e3, squeeze_output=True)

    # a) projection error
    err_f1 = [np.max(np.abs(exact(e1, e2, e3) - field_v)) for exact, field_v in zip([f, f, f], field_f1_vals)]
    print(f'{err_f1 = }')
    assert np.max(err_f1) < .05

    # b) commuting property
    df1_h = derham.curl.dot(f1_h)
    assert np.allclose(df1_h.toarray(), proj_of_curl_fff.toarray())

    # c) derivative error
    field_df1 = derham.create_field('df1', 'Hdiv')
    field_df1.vector = df1_h
    field_df1_vals = field_df1(e1, e2, e3, squeeze_output=True)

    err_df1 = [np.max(np.abs(exact(e1, e2, e3) - field_v)) for exact, field_v in zip(curl_f, field_df1_vals)]
    print(f'{err_df1 = }')
    assert np.max(err_df1) < .4

    # d) plotting
    plt.figure(figsize=(8, 12))
    plt.subplot(3, 1, 1)
    plt.plot(e, f(e1, e2, e3), 'o')
    plt.plot(e, field_f1_vals[c])
    plt.title('all components fun')
    plt.xlabel(f'eta{c + 1}')

    plt.subplot(3, 1, 2)
    plt.plot(e, curl_f[(c + 1) % 3](e1, e2, e3), 'o')
    plt.plot(e, field_df1_vals[(c + 1) % 3])
    plt.title(f'curl comp {(c + 1) % 3}')
    
    plt.subplot(3, 1, 3)
    plt.plot(e, curl_f[(c + 2) % 3](e1, e2, e3), 'o')
    plt.plot(e, field_df1_vals[(c + 2) % 3])
    plt.title(f'curl comp {(c + 2) % 3}')
    
    plt.subplots_adjust(wspace=1.0, hspace=0.4)

    ##########
    # 2-form #
    ##########
    f2_h = derham.P['2']((f, f, f))

    field_f2 = derham.create_field('f2', 'Hdiv')
    field_f2.vector = f2_h
    field_f2_vals = field_f2(e1, e2, e3, squeeze_output=True)

    # a) projection error
    err_f2 = [np.max(np.abs(exact(e1, e2, e3) - field_v)) for exact, field_v in zip([f, f, f], field_f2_vals)]
    print(f'{err_f2 = }')
    assert np.max(err_f2) < .05

    # b) commuting property
    df2_h = derham.div.dot(f2_h)
    assert np.allclose(df2_h.toarray(), proj_of_div_fff.toarray())

    # c) derivative error
    field_df2 = derham.create_field('df2', 'L2')
    field_df2.vector = df2_h
    field_df2_vals = field_df2(e1, e2, e3, squeeze_output=True)

    err_df2 = np.max(np.abs(div_f(e1, e2, e3) - field_df2_vals)) 
    print(f'{err_df2 = }')
    assert np.max(err_df2) < .4

    # d) plotting
    plt.figure(figsize=(8, 12))
    plt.subplot(2, 1, 1)
    plt.plot(e, f(e1, e2, e3), 'o')
    plt.plot(e, field_f2_vals[c])
    plt.title('all components fun')
    plt.xlabel(f'eta{c + 1}')

    plt.subplot(2, 1, 2)
    plt.plot(e, div_f(e1, e2, e3), 'o')
    plt.plot(e, field_df2_vals)
    plt.title(f'div')
    
    plt.subplots_adjust(wspace=1.0, hspace=0.4)
    
    ##########
    # 3-form #
    ##########
    f3_h = derham.P['3'](f)

    field_f3 = derham.create_field('f3', 'L2')
    field_f3.vector = f3_h
    field_f3_vals = field_f3(e1, e2, e3, squeeze_output=True)

    # a) projection error
    err_f3 = np.max(np.abs(f(e1, e2, e3) - field_f3_vals))
    print(f'{err_f3 = }')
    assert err_f3 < 0.05

    # d) plotting
    plt.figure(figsize=(8, 12))
    plt.subplot(2, 1, 1)
    plt.plot(e, f(e1, e2, e3), 'o')
    plt.plot(e, field_f3_vals)
    plt.title('fun')
    plt.xlabel(f'eta{c + 1}')
    
    plt.subplots_adjust(wspace=1.0, hspace=0.4)

    if do_plot:
        plt.show()

if __name__ == '__main__':
    test_lowdim_derham([64, 1, 1], [1, 1, 1], [True, True, True], do_plot=False)
    test_lowdim_derham([1, 64, 1], [1, 1, 1], [True, True, True], do_plot=False)
    test_lowdim_derham([1, 1, 64], [1, 1, 1], [True, True, True], do_plot=False)
